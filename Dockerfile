FROM ubuntu:bionic AS build
LABEL maintainer=georgiev@iskrauraltel.ru

ENV HELM_VERSION=3.8.2
ENV RELEASE_ROOT="https://get.helm.sh"
ENV RELEASE_FILE="helm-v${HELM_VERSION}-linux-amd64.tar.gz"

RUN apt-get update && apt-get install curl -y && \
    curl -L ${RELEASE_ROOT}/${RELEASE_FILE} | tar xvz && \
    mv linux-amd64/helm /usr/bin/helm && \
    chmod +x /usr/bin/helm && \
    curl -sSL -o /usr/local/bin/argocd https://github.com/argoproj/argo-cd/releases/latest/download/argocd-linux-amd64 && \
    chmod +x /usr/local/bin/argocd && \
    curl -LO https://storage.googleapis.com/kubernetes-release/release/v1.24.0/bin/linux/amd64/kubectl && \
    chmod +x ./kubectl && \
    mv ./kubectl /usr/bin/kubectl 

FROM ubuntu:bionic
COPY --from=build /usr/bin/helm /usr/bin/helm
COPY --from=build /usr/bin/kubectl /usr/bin/kubectl
COPY --from=build /usr/local/bin/argocd /usr/bin/argocd
RUN apt-get update && apt-get install curl -y && \ 
    curl https://storage.yandexcloud.net/yandexcloud-yc/install.sh | bash -s -- -i /usr/ -n
CMD ["welcome"]